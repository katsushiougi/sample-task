# Sample Grunt task

## 準備

```
// プロジェクトのディレクトリにコマンドラインから移動して
npm install
```

## Grunt 実行

```
grunt start
```

- ローカルサーバー `http://localhost:3000` が立ち上がります。
- ルート・ディレクトリは `dist` 以下になります。

## 一度だけGruntタスクを実行

```
grunt build
```

- ローカルサーバーは起動しない。

## タスクの内容

> 作業はすべて src ディレクトリ内でおこなってください。

以下の内容を自動化しています。

1. `dist`ディレクトリを削除
2. sass のコンパイル。`src/assets/css`内にcssが保存されます。
3. ベンダープリフィックスを自動で追加(autoprefixer)。
4. `src/*.html`と`src/assets/`以下を`dist`ディレクトリにコピー。distディレクトリは毎回削除され、srcディレクトリから常にコピーされます。distディレクトリに画像などを保存するとすべて削除されてしまいます。srcディレクトリに保存してください。

※ autoprefixerとは？

```
.hoge {
  transform: translate3d(0, 0, 0);
}
// 例えば上のcssがautoprefixerを通すと以下になります。
.hoge {
  -webkit-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0); }
```

