pkg = require './package.json'

###
grunt設定
###

module.exports = (grunt) ->

  for taskName of pkg.devDependencies
    if taskName.substring(0, 6) == 'grunt-' then grunt.loadNpmTasks taskName

  config =

    pkg: grunt.file.readJSON('package.json')

    ###
    grunt-contrib-sass
    ###
    sass:
      dist:
        expand: true
        cwd   : 'src/scss'
        src   : ['**/!(_)*.scss']
        dest  : 'src/assets/css'
        ext   : '.css'

    ###
    grunt-autoprefixer
    ###
    autoprefixer:
      dist:
        src: 'src/assets/css/style.css'

    ###
    grunt-contrib-clean
    ###
    clean: [ 'dist' ]

    ###
    grunt-contrib-copy
    ###
    copy:
      assets:
        expand : true
        cwd    : 'src'
        src    : [
          '**/*.html'
          'assets/**/*'
        ]
        dest : 'dist'

    ###
    grunt-notify
    ###
    notify:
      build:
        options:
          title  : 'ビルド完了'
          message: 'タスクが正常終了しました。'
      watch:
        options:
          title  : '監視開始'
          message: 'ローカルサーバーを起動しました'

    ###
    grunt-contrib-connect
    ###
    connect:
      server:
        options:
          port    : 3000
          base    : 'dist'
          hostname: '0.0.0.0'
          spawn   : false

    ###
    grunt-contrib-watch
    ###
    watch:
      options:
        livereload: true
        spawn     : false
      html:
        files: [ 'src/**/*.html' ]
        tasks: [ 'copy', 'notify:build' ]
      css:
        files: [ 'src/scss/**/*.scss' ]
        tasks: [ 'sass', 'autoprefixer', 'copy', 'notify:build' ]
      js:
        files: [ 'src/assets/js/**/*.js' ]
        tasks: [ 'copy', 'notify:build' ]

  grunt.initConfig( config )

  # grunt start
  grunt.registerTask 'start', [
    'clean'
    'sass'
    'autoprefixer'
    'copy'
    'notify:watch'
    'connect'
    'watch'
  ]

  # grunt build
  grunt.registerTask 'build', [
    'clean'
    'sass'
    'autoprefixer'
    'copy'
    'notify:build'
  ]